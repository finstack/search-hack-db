const functions = require('firebase-functions')
const admin = require('firebase-admin')

// const cors = require('cors')({ origin: true })
// const cors = require('cors')({
//     origin: '*',
//     allowedHeaders: ['Content-Type', 'Authorization']
// });

admin.initializeApp(functions.config().firebase)

// onRequest example!
// exports.helloWorld = functions.https.onRequest((request, response) => {
//     return cors(request, response, () => {
//         // :: response.send must return an object
//         return response.send({
//             // :: Object must have a key named 'data'
//             data: 'Hello World!'
//         });
//     })
// });

// OnCall example!
exports.helloWorld = functions.https.onCall((data, context) => {

	// Checking that the user is authenticated.
	if (!context.auth) {
	    // Throwing an HttpsError so that the client gets the error details.
	    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
	      'while authenticated.');
	}

    const text = data.text
	const uid = context.auth.uid;
	const name = context.auth.token.name || null
	const picture = context.auth.token.picture || null
	const email = context.auth.token.email || null
    console.log(text)
    console.log(uid)
    return {
        mytext: 'Hello BKK!',
        uid: uid,
        email: email
	}

})