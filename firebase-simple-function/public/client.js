function mySearch(){

	// We login first as Greg
	firebase.auth().signInWithEmailAndPassword('greg@finstack.ch', '123456').then((cred) => {
		console.log(cred.user.email + ' has logged in with success')

		let helloWorld = firebase.functions().httpsCallable('helloWorld');
		helloWorld({text: "This is a test"}, {auth: {uid: 'abcd'}}).then((result) => {
		  // :: Read result of the Cloud Function.
		  let sanitizedMessage = result.data;
		  console.log(result);
		  // ...
		}).catch(err => {
		    console.log(err);
		});
	})

}