const functions = require('firebase-functions');
const express = require('express');
const engines = require('consolidate')
const app = express();

var bodyParser = require('body-parser');
var path = require('path');
var qs = require('querystring');
var fs = require('fs');



// var app = express();
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extend:true}));
app.use(bodyParser.json())

// app.use(express.static(path.join(__dirname, '/public')));

app.get('/', function(req, res) {
 res.render('index', { searchedData: '', dbResult: '' });
});

exports.app = functions.https.onRequest(app);


