// from doc
// https://firebase.google.com/docs/hosting/functions?utm_campaign=featureoverview_education_hosting_en_08-01-17&utm_source=Firebase&utm_medium=yt-desc

// test url:
// https://us-central1-simpleapp-b2460.cloudfunctions.net/app/api1

const functions = require('firebase-functions');
const express = require('express');
const app = express();
// const request = require('request-promise')

var admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const cors = require('cors')({origin: true});
app.use(cors);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase 2!");
// });

app.get('/', (req, res) => {
  const date = new Date();
  const hours = (date.getHours() % 12) + 1;  // London is UTC + 1hr;
  //const myvar = functions.config().someservice.id;
  res.send(`
    <!doctype html>
    <head>
      <title>Time</title>
      <link rel="stylesheet" href="/style.css">
      <script src="/script.js"></script>
    </head>
    <body>
      <p>My Var:</br> hours=${hours} </br> In London , the clock strikes:
        <span id="bongs">${'BONG '.repeat(hours)}</span></p>
      <button onClick="refresh(this)">Refresh</button>
    </body>
  </html>`);
});

app.get('/api', (req, res) => {
  const date = new Date();
  const hours = (date.getHours() % 12) + 1;  // London is UTC + 1hr;
  res.json({bongs: 'BONG '.repeat(hours)});
});

exports.app = functions.https.onRequest(app);


