var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var qs = require('querystring');
var fs = require('fs');

// Import the Google Cloud client library
const {BigQuery} = require('@google-cloud/bigquery');

var app = express();
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extend:true}));
app.use(bodyParser.json())


async function mybigquery (username){
// BIGQUERY //
  // Create a client
  const bigqueryClient = new BigQuery();

  const query = `
    SELECT * FROM gregtest123.greguserpass 
    WHERE username LIKE @username
    `;
  const options = {
    query: query,
    // Location must match that of the dataset(s) referenced in the query.
    location: 'EU',
    params: {username: '%' + username + '%', min_word_count: 250},
  };

  // Run the query as a job
  const [job] = await bigqueryClient.createQueryJob(options);
  console.log(`Job ${job.id} started.`);

  // Wait for the query to finish
  const [rows] = await job.getQueryResults();

  // Print the results
  rows.forEach(row => console.log(row));
  return rows;
};



app.get('/', function(req, res) {
 res.render('search', { searchedData: '', dbResult: '' });
});

app.post('/', async function(req, res) {
  var username = req.body.username
  
  mybigquery(username).then(dbResult => {
    // Render result to html page with vars
    res.render('search', { 
      searchedData: 'Result(s) for your search: "' + username + '"',
      dbResult: dbResult
    });
  });

});

app.listen(3000);