## Search hacked db


A simple page to search username:password from a compilation of knowned hacked password.
More context [here](https://blog.appsecco.com/using-google-cloud-platform-to-store-and-query-1-4-billion-usernames-and-passwords-6cac572f5a29?gi=52d797019ad4)

### Infra

user >> Nodejs (local) >> GCP (via service user cred) >> Database (bigquery)


### Initialisation
```
brew install nodejs
npm install --save express
npm install --save ejs
npm install --save @google-cloud/bigquery
npm install --save @google-cloud/storage
npm install --save nodemon
```

In GCP, create service account:`gregtest-bigquery` with role: `bigquery viever + job user `, download json and run the command on your laptop:
```
export GOOGLE_APPLICATION_CREDENTIALS="/Users/greg/dev/cred/Pawnedpass-7c69b25edc74.json"
```

## Run app

Run locally: `nodemon app.js`

Then access http://localhost:3000


## Deploy on firebase

Setup:

Create a new project in firebase UI
```
npm install --global firebase-tools
firebase login
firebase init   # Select hosting, function
firebase use --add search-hacked-db

firebase serve
firebase deploy
```


