var express = require ('express');
var qs = require('querystring');
var fs = require('fs');

// Import the Google Cloud client library
const {BigQuery} = require('@google-cloud/bigquery');

var postHTML = 
'<html><head><title>Search the PWD database</title></head>' +
'<body>' +
'<form method="post">' +
'Input: <input name="input"><br>' +
'<input type="submit">' +
'</form>' +
'</body></html>';


var http = require('http');  

http.createServer(async function(req, res) {  
  res.writeHead(200, {
    'Content-Type': 'text/html'
  });

  // Create a client
  const bigqueryClient = new BigQuery();

  const query = `SELECT *
    FROM \`gregtest123.greguserpass\`
    `;
  const options = {
    query: query,
    // Location must match that of the dataset(s) referenced in the query.
    location: 'EU',
  };

  // Run the query as a job
  const [job] = await bigqueryClient.createQueryJob(options);
  console.log(`Job ${job.id} started.`);

  // Wait for the query to finish
  const [rows] = await job.getQueryResults();

  // Print the results
  //console.log('Rows:');
  rows.forEach(row => console.log(row));
  rows.forEach(row => res.write(JSON.stringify(row)+'<br />'));
  res.end();

}).listen(3000, '127.0.0.1');

console.log('Server running at http://127.0.0.1:3000');




