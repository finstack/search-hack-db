const functions = require('firebase-functions');
const express = require('express');
const engines = require('consolidate')
const app = express();

// const firebaseApp = firebase.initializeApp(
// 	functions.config().firebase
// );

var bodyParser = require('body-parser');
var path = require('path');
var qs = require('querystring');
var fs = require('fs');

// Import the Google Cloud client library
const {BigQuery} = require('@google-cloud/bigquery');

// var app = express();
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extend:true}));
app.use(bodyParser.json())

// app.use(express.static(path.join(__dirname, '/public')));

async function mybigquery (username){
// BIGQUERY //
  // Create a client
  const bigqueryClient = new BigQuery();

  const query = `
    SELECT * FROM test_db_pass.test_pass_table 
    WHERE username LIKE @username
    `;
  const options = {
    query: query,
    // Location must match that of the dataset(s) referenced in the query.
    location: 'EU',
    params: {username: '%' + username + '%', min_word_count: 250},
  };

  // Run the query as a job
  const [job] = await bigqueryClient.createQueryJob(options);
  console.log(`Job ${job.id} started.`);

  // Wait for the query to finish
  const [rows] = await job.getQueryResults();

  // Print the results
  rows.forEach(row => console.log(row));
  return rows;
};


app.get('/', function(req, res) {
 res.render('index', { nbResult: '', searchedData: '', dbResult: '' });
});

app.post('/', async function(req, res) {
  var username = req.body.username
  
  console.log(username);

  mybigquery(username).then(dbResult => {
    // Render result to html page with vars
    res.render('index', { 
      nbResult: dbResult.length + ' result(s) [login : password] for user  ',
      searchedData: '"' + username + '"',
      dbResult: dbResult
    });
  });

});

exports.app = functions.https.onRequest(app);


